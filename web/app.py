from flask import Flask, render_template, request, send_from_directory
import os

app = Flask(__name__)

@app.route("/")
def hello():
    return "HI WELCOME TO THE SITE"

@app.route("/<name>/")
def htmlresponse(name):
    if os.path.exists('templates/' + name):
        if 'html' in request.environ['PATH_INFO']:
            return render_template('/' + name)
        elif 'css' in request.environ['PATH_INFO']:
            return send_from_directory('templates', name)
    elif ('~' in request.environ['PATH_INFO']) or ('..' in request.environ['PATH_INFO']) or ('//' in request.environ['PATH_INFO']):
        return render_template("403.html")


@app.errorhandler(404)
def handle_notfound(e):
    if ('~' in request.environ['PATH_INFO']) or ('..' in request.environ['PATH_INFO']) or ('//' in request.environ['PATH_INFO']):
        return render_template("403.html")
    return render_template("404.html"), 404

@app.errorhandler(403)
def handle_forbid(e):
    return render_template("403.html"), 403

if __name__ == "__main__":
    app.run(debug="true", host='0.0.0.0')